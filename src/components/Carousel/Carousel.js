/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import CarouselDetails from '../CarouselDetails';
import Slider from 'react-slick';
import cx from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Carousel.css';
import bgImage01 from './background01.jpg';
import bgImage02 from './background02.jpg';

class Carousel extends React.Component {
  render() {
  let settings = {
      arrows: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      className: s.slideDestaqueHomeTop
    }
    return (
      <div className={s.container} >
        <Slider className={s.sliderHomeTop} {...settings}>

            <div>
              <img  src={bgImage01} alt=""/>
              <CarouselDetails />
            </div>

            <div>
              <img src={bgImage02} alt=""/>
              <CarouselDetails />
            </div>

        </Slider>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
      </div>
    );
  }
}

export default withStyles(s)(Carousel);
