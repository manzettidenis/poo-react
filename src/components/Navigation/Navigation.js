/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import cx from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Navigation.css';
import Link from '../Link';

class Navigation extends React.Component {
  render() {
    return (
      <ul className={s.root} role="navigation">
        <li><Link className={s.link} to="/about">HOME</Link></li>
        <li><Link className={s.link} to="/contact">COMPRE</Link>
          <ul className={s.dropDown}>
              <li><Link className={s.link} to="/#">Sub 1</Link></li>
              <li><Link className={s.link} to="/#">Sub 2</Link></li>
              <li><Link className={s.link} to="/#">Sub 3</Link></li>
          </ul>
        </li>
        <li><Link className={s.link} to="/contact">COMPARE</Link></li>
        <li><Link className={s.link} to="/contact">VENDA</Link></li>
        <li><Link className={s.link} to="/contact">BLOG</Link></li>
        <li><Link className={s.link} to="/contact">CONTATO</Link></li>
        <li><Link className={cx(s.link, s.highlight)} to="/register">Login</Link></li>
      </ul>
    );
  }
}

export default withStyles(s)(Navigation);
