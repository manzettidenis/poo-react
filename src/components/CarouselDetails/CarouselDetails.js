/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import cx from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CarouselDetails.css';

class CarouselDetails extends React.Component {
  render() {

    return (
      <div className={s.sliderHomeTopDetails} >

        <div className={s.price}>
          R$24.000
        </div>

        <div className={s.title}>
          TOP CARS: MERCEDES S CLASS
        </div>

        <div className={s.description}>
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem eriam, eaque ipsa quae ab illo inventore veritatis..
        </div>

        <button className={s.buttonSliderTop}>Detalhes</button>
      </div>
    );
  }
}

export default withStyles(s)(CarouselDetails);
