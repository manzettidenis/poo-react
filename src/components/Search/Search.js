/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import cx from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Search.css';
import Link from '../Link';

class Search extends React.Component {
  render() {
    return (
      <div className={s.root} >
          <ul>
            <li>
              <ul class="dropDown">
                <li>
                    <form className={s.searchFormTop}>
                        <select className={s.searchAutoInput} name="Brand" placeholder="Marca">
                          <option value="Marca">Marca</option>
                          <option value="valor1">valor1</option>
                          <option value="valor2">valor2</option>
                        </select>
                        <div className={s.spaceInputs}></div>
                        <select className={s.searchAutoInput} name="Model" placeholder="Modelo">
                          <option value="Modelo">Modelo</option>
                          <option value="valor1">valor1</option>
                          <option value="valor2">valor2</option>
                        </select>
                        <div className={s.spaceInputs}></div>
                        <select className={s.searchAutoInput} name="Year" placeholder="Ano">
                          <option value="Ano">Ano</option>
                          <option value="valor1">valor1</option>
                          <option value="valor2">valor2</option>
                        </select>
                        <div className={s.spaceInputs}></div>
                        <input className={s.searchAutoInput} type="range" name="rangeInput" min="0" max="100"/>
                        <div className={s.spaceInputs}></div>
                        <button className={s.searchAutoButton}>Buscar</button>
                    </form>

                </li>
              </ul>
            </li>
          </ul>
      </div>
    );
  }
}

export default withStyles(s)(Search);
